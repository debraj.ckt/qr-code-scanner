import { Component, OnInit } from '@angular/core';
import { BarcodeScanner} from '@awesome-cordova-plugins/barcode-scanner/ngx';
import { AlertController} from '@ionic/angular';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { Router } from '@angular/router';
@Component({
  selector: 'app-qr-generator',
  templateUrl: './qr-generator.page.html',
  styleUrls: ['./qr-generator.page.scss'],
})
export class QrGeneratorPage implements OnInit {
  encodedData: any;

  constructor(
    private barcodeScanner: BarcodeScanner,
    private alertController: AlertController,
    private socialSharing: SocialSharing,
    private route:Router

    ) { }

  ngOnInit() {
  }

  generateQrCode(){
    const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    const charactersLength = characters.length;
    for ( let i = 0; i < 5; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    this.encodedData = result;
  }

  shareQrCode(parent: any){
    let parentElement = parent.qrcElement.nativeElement.querySelector("img").src;
    console.log(parentElement);
    if (parentElement) {
      let file=[];
      file.push(parentElement);
      var options = {
        message: 'Hi, i am sharing you the qr code',
        subject: 'Social Share',
        files:file,
        chooserTitle: 'Share'
      }
      this.socialSharing.shareWithOptions(options);
    }
  }

  gotoScanQrCode(){
    this.route.navigateByUrl('qr-scanner');
  }
}
