import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  lform: FormGroup | any;
  constructor(
    private formBuilder: FormBuilder,
    private toastController: ToastController,
    private route:Router
  ) { }

  ngOnInit() {
    this.lform = this.formBuilder.group({
      email: [null, [Validators.required,Validators.pattern(/^(\s?[^\s,]+@[^\s,]+\.[^\s,]+\s?,)*(\s?[^\s,]+@[^\s,]+\.[^\s,]+)$/g)]],
      password: [null, [Validators.required]],
    });
  }


  async onSubmit() {
    if (this.lform.valid) {
      if (this.lform.value.email != 'test@test.com' || this.lform.value.password != '8256455') {
        const toast = await this.toastController.create({
          message: 'Username or Password you have entered is invalid',
          duration: 3000,
        });
        await toast.present();
        return;
      }
      this.route.navigateByUrl('qr-scanner');
    } else {
      this.markFormGroupTouched(this.lform);
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control:any) => {
      control.markAsTouched();
      if (control.controls) {
        control.controls.forEach((c:any) => this.markFormGroupTouched(c));
      }
    });
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field)?.valid && (form.get(field)?.dirty || form.get(field)?.touched);
  }
}
