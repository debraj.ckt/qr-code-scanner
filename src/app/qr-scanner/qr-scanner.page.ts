import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BarcodeScanner, BarcodeScannerOptions } from '@awesome-cordova-plugins/barcode-scanner/ngx';

@Component({
  selector: 'app-qr-scanner',
  templateUrl: './qr-scanner.page.html',
  styleUrls: ['./qr-scanner.page.scss'],
})
export class QrScannerPage implements OnInit {
  scannedData: any;
  constructor(
    private barcodeScanner: BarcodeScanner,
    private route:Router
    ) { }

  ngOnInit() {
  }

  StartScanBarcode() {
    const options: BarcodeScannerOptions = {
      preferFrontCamera: false,
      showTorchButton: true,
      torchOn: false,
      prompt: 'Place a barcode inside the scan area',
      resultDisplayDuration: 500,
      formats: 'QR_CODE',
      orientation: 'portrait',
    };
    this.barcodeScanner.scan(options).then(barcodeData => {
      this.scannedData = barcodeData;

    }).catch(err => {
      console.log('Error', err);
    });
  }

  gotoQrGeneratePage(){
    this.route.navigateByUrl('qr-generator');
  }
}
